# Topic Twister MVP Test

This is a test project to .....

# Table of Contents

- [Contributing](#contributing)
- [Authors](#authors)

## Prerequisites

## [Contributing](CONTRIBUTING.md)

Our contributing guidelines can be found here [contributing.md](CONTRIBUTING.md).

## Project Folder Structure

Considering the bellow folder structure:

```
project root
│
└───docs
│   │
│   └───subfolder1
│       │   README.md
│       │   SUMMARY.md
│       │   section1.md
│       │   section2.md
│       │   ...
│   └───subfolder2
│       │   README.md
│       │   ...
│   └───subfolder3
│       │   SUMMARY.md
│       │   ...
|   ...
```

The summary should look something like this:

## Authors

- [Emanuel Jara](https://github.com/fruei)
- [Elio Herrera](https://github.com/eliosinhache)
- [Nicolás Kotolenko](https://github.com/Ripley-426)
- [Martin Pellicer](https://github.com/martinpelli)
- [Mariano Wiñar](https://github.com/winarmarianoj)
- [Daniel Alzueta](https://github.com/dalzueta)
