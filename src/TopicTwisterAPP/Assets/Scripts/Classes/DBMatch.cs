﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Classes
{
    [Serializable]
    public class DBMatch
    {
        public int matchid;
        public int playerAID;
        public int playerBID;
        public List<DBRound> rounds;
        public int winner;

        public bool IsFinished()
        {
            return rounds[rounds.Count-1].turn == "FINISHED";
        }

        public int GetCurrentPlayer()
        {
            DBRound currentRound = rounds.First(round => round.turn != "FINISHED");
            
            {
                if (currentRound.roundNumber % 2 == 0)
                {
                    return currentRound.turn == "FIRST" ? playerBID : playerAID;
                }
                return currentRound.turn == "FIRST" ? playerAID : playerBID;
            }
        }
        
        public int GetRoundNumberToPlay()
        {
            for (int i = 0; i < rounds.Count; i++)
            {
                if (rounds[i].turn == "FINISHED") continue;
                return i;
            }

            return 2;
        }

        public int GetPlayedRoundNumber()
        {
            //TODO fijarse si los 2 primeros rounds ya tiene ganador, entonces devuelva a la vista round 2 con return 1
            for (int i = 0; i < rounds.Count; i++)
            {
                if (rounds[i].turn == "FINISHED") continue;
                if (!rounds[i].playerAWordsValidation.Any() && !rounds[i].playerBWordsValidation.Any())
                {
                    return i - 1;
                }
                return i;
            }

            return 2;
        }

        public DBRound GetRoundToPlay()
        {
            return rounds[GetRoundNumberToPlay()];
        }

        public DBRound GetPlayedRound()
        {
            return rounds[GetPlayedRoundNumber()];
        }

        public string GetScore(int currentPlayerID)
        {
            int playerAScore = 0;
            int playerBScore = 0;
            foreach (DBRound round in rounds)
            {
                switch (round.roundWinner)
                {
                    case "PLAYERA":
                        playerAScore++;
                        break;
                    case "PLAYERB":
                        playerBScore++;
                        break;
                }
            }
            if (currentPlayerID == playerAID)
            {
                return playerAScore + " - " + playerBScore;
            }
            
            return playerBScore + " - " + playerAScore;
        }
        
        public bool IsPlayerA(int playerID)
        {
            return playerAID == playerID;
        }
    }
}
