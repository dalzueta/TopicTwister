﻿using System;
using System.Threading.Tasks;

namespace Classes
{
    [Serializable]
    public class LoginHandler
    {
        private DBLogin _loginCredentials = new DBLogin();
        private DBRegister _registerCredentials = new DBRegister();
        
        private RequestsHandler _requestsHandler = new RequestsHandler();
        
        public async Task<DBPlayer> SendLogin(string email, string password)
        {
            _loginCredentials.email = email;
            _loginCredentials.password = password;
            return await _requestsHandler.LoginPlayer(_loginCredentials);
        }


        public DBPlayer SendRegister(string email, string password, string name)
        {
            _registerCredentials.email = email;
            _registerCredentials.password = password;
            _registerCredentials.name = name;

            return _requestsHandler.RegisterPlayer(_registerCredentials);
        }
    }
}