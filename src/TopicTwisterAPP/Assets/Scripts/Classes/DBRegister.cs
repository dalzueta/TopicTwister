﻿using System;

namespace Classes
{
    [Serializable]
    public class DBRegister
    {
        public string email { get; set; }
        public string password { get; set; }
        public string name { get; set; }
    }
}