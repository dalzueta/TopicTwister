﻿using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Classes
{
    public class LoginAPICallerUniTask
    {
        private const string APIURL = "https://topictwisterequipocinco.herokuapp.com/player/";
        private const string LoginURL = APIURL + "login";
        
        public async UniTask<DBPlayer> LoginPlayer(string email, string password)
        {
            UnityWebRequest request = new UnityWebRequest(LoginURL, "POST");
            string json = CreateLoginJson(email, password);
            UploadHandler upHandler = CreateJSONUploadHandlerWithThisBody(json);
            request.uploadHandler = upHandler;
            DownloadHandler downHandler = new DownloadHandlerBuffer();
            request.downloadHandler = downHandler;

            return await LoginPlayerAsync(request);
        }

        private string CreateLoginJson(string email, string password)
        {
            return "{\"email\":\"" + email + "\"," +
                   "\"password\":\"" + password + "\"}";
        }

        private UploadHandler CreateJSONUploadHandlerWithThisBody(string json)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(json);
            UploadHandler upHandler = new UploadHandlerRaw(data);
            upHandler.contentType = "application/json";
            return upHandler;
        }

        private async UniTask<DBPlayer> LoginPlayerAsync(UnityWebRequest req)
        {
            UnityWebRequest op = await req.SendWebRequest();
            DBPlayer player = JsonUtility.FromJson<DBPlayer>(op.downloadHandler.text);
            return player;
        }
    }
}