﻿namespace UI.Scenes.MainMenu
{
    public interface IObserver
    {
        void ReceiveUpdate();
    }
}