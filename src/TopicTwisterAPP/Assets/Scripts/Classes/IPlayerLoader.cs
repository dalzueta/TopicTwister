﻿namespace Classes
{
    public interface IPlayerLoader
    {
        Player GetPlayer(string lastPlayer);
    }
}