﻿namespace Classes
{
    public static class DataManager
    {
        public static DBPlayer CurrentPlayer { get; set; }
        public static DBMatch CurrentMatch { get; set; }
    
        public static bool IsPlayerA()
        {
            return CurrentMatch.playerAID == CurrentPlayer.id;
        }
    }
}
