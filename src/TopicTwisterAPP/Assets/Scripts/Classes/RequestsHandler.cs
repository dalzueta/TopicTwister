﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Classes
{
	public class RequestsHandler {

		private DBMatch _match;

		private DBPlayer _player;


		private void LogMessage(string title, string message) {
			Debug.Log(title+": "+message);
		}

		public List<DBMatch> GetAllMatchesFromPlayer(int playerID)
		{
			DBMatchList result = APIHelper.GetAllMatchesFromPlayer(playerID);

			return result.matches;
		}
	
		public DBMatch GetMatchWithID(int idMatch)
		{
			return APIHelper.GetNewMatch(idMatch);
		}

		public DBMatch StartNewMatch(int playerID)
		{
			return APIHelper.StartNewMatch(playerID);
		}

		public DBMatch AddWordsToMatch(List<string> wordList)
		{
			return APIHelper.AddWordsToMatch(wordList);
		}

		public DBMatch StartRematch(int myPlayerID, int opponentID)
		{
			return APIHelper.StartRematch(myPlayerID, opponentID);
		}

		public async Task<DBPlayer> LoginPlayer(DBLogin loginCredentials)
		{
			return await APIHelper.LoginPlayer(loginCredentials);
		}

		public DBPlayer RegisterPlayer(DBRegister registerCredentials)
		{
			return APIHelper.RegisterPlayer(registerCredentials);
		}

		public int LoadTimeFromDB(int matchRoundID)
		{
			return APIHelper.LoadTimeFromDB(matchRoundID);
		}

		public void SaveTimeInDB(int matchRoundID, int time)
		{
			APIHelper.SaveTimeInDB(matchRoundID, time);
		}

		public DBPlayer GetPlayerName(int playerID)
		{
			return APIHelper.GetPlayer(playerID);
		}
	}
}