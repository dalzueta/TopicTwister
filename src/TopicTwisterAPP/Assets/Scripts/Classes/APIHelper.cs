﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Classes;

public static class APIHelper
{
    private const string MatchURL = "https://topic-twister-api.herokuapp.com/";
    private const string LoginURL = "https://topictwisterequipocinco.herokuapp.com/player/";
    private const string TimerURL = "https://timer-tt-api.herokuapp.com/";

    public static async Task<DBMatch> GetNewMatch()
    {
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(
            MatchURL+ "match/GetMatchWithID?matchID=103");
        Task<WebResponse> response = request.GetResponseAsync();
        await response;

        StreamReader reader = new StreamReader(response.GetAwaiter().GetResult().GetResponseStream());
        var json = reader.ReadToEnd();

        return JsonUtility.FromJson<DBMatch>(json);
    }
    
    public static DBMatch GetNewMatch(int idmatch)
    {
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(MatchURL + "match/GetMatchWithID?matchID="+ idmatch);
        HttpWebResponse response =(HttpWebResponse) request.GetResponse();

        StreamReader reader = new StreamReader(response.GetResponseStream());
        var json = reader.ReadToEnd();

        return JsonUtility.FromJson<DBMatch>(json);
    }
    
    public static DBMatchList GetAllMatchesFromPlayer(int playerID)
    {
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(MatchURL + $"match/GetMatchesOfPlayer?playerID={playerID}");
        HttpWebResponse response = (HttpWebResponse) request.GetResponse();

        StreamReader reader = new StreamReader(response.GetResponseStream());
        var json = reader.ReadToEnd();

        json = "{\"matches\": " + json + "}"; 

        return JsonUtility.FromJson<DBMatchList>(json);
    }

    public static DBMatch StartNewMatch(int playerID)
    {
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(
            MatchURL + "match/startNewMatch?playerID=" + playerID);
        HttpWebResponse response = (HttpWebResponse) request.GetResponse();
        
        StreamReader reader = new StreamReader(response.GetResponseStream());
        var json = reader.ReadToEnd();

        return JsonUtility.FromJson<DBMatch>(json);
    }

    public static DBMatch AddWordsToMatch(List<string> wordList)
    {
        var words = String.Join(",", wordList.ToArray());
        string completeURL = MatchURL + "match/addWordsToMatch?playerID=" + DataManager.CurrentPlayer.id +
                             "&matchID=" + DataManager.CurrentMatch.matchid + "&words=" + words;
        
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(completeURL);

        request.Method = "POST";
        
        HttpWebResponse response = (HttpWebResponse) request.GetResponse();

        StreamReader reader = new StreamReader(response.GetResponseStream());
        var json = reader.ReadToEnd();

        return JsonUtility.FromJson<DBMatch>(json);
    }

    public static DBMatch StartRematch(int myPlayerID, int opponentID)
    {
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(
            MatchURL + "match/startRematch?playerID=" + myPlayerID + "&opponentID=" + opponentID);
        HttpWebResponse response = (HttpWebResponse) request.GetResponse();
        
        StreamReader reader = new StreamReader(response.GetResponseStream());
        var json = reader.ReadToEnd();

        return JsonUtility.FromJson<DBMatch>(json);
    }

    public static async Task<DBPlayer> LoginPlayer(DBLogin credential)
    {
        string json = "{\"email\":\"" + credential.email + "\"," +
                      "\"password\":\"" + credential.password + "\"}";

        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(LoginURL + "login");
        
        request.ContentType = "application/json";
        request.Method = "POST";

        request.ContentLength = json.Length;

        byte[] data = Encoding.ASCII.GetBytes(json);

        
        Stream str = await request.GetRequestStreamAsync();
        str.Write(data,0, json.Length);

        var httpResponse = (HttpWebResponse) request.GetResponse();
        
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            var result = streamReader.ReadToEnd();
            var player = JsonUtility.FromJson<DBPlayer>(result);
            return player;
        }
    }

    public static DBPlayer RegisterPlayer(DBRegister credentials)
    {
        string json = "{\"name\":\"" + credentials.name + "\"," +
                      "\"email\":\"" + credentials.email + "\"," +
                      "\"password\":\"" + credentials.password + "\"}";

        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(LoginURL);
        
        request.ContentType = "application/json";
        request.Method = "POST";

        request.ContentLength = json.Length;

        byte[] data = Encoding.ASCII.GetBytes(json);

        using (Stream stream = request.GetRequestStream()) 
        {
            stream.Write(data,0, json.Length);
        }

        var httpResponse = (HttpWebResponse) request.GetResponse();
        
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            var result = streamReader.ReadToEnd();
            return JsonUtility.FromJson<DBPlayer>(result);
        }
    }
    
    public static DBPlayer GetPlayer(int playerID)
    {
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(
            LoginURL + playerID);
        HttpWebResponse response = (HttpWebResponse) request.GetResponse();
        
        StreamReader reader = new StreamReader(response.GetResponseStream());
        var json = reader.ReadToEnd();

        return JsonUtility.FromJson<DBPlayer>(json);
    }

    public static void SaveTimeInDB(int matchRoundID, int time)
    {
        string completeURL = TimerURL + $"saveMatchRoundTime?matchRoundID={matchRoundID}&time={time}";
        
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(completeURL);

        request.Method = "POST";
        
        request.GetResponse();
    }
    
    public static int LoadTimeFromDB(int matchRoundID)
    {
        string completeURL = TimerURL + $"getMatchRoundTime?matchRoundID={matchRoundID}";
        
        HttpWebRequest request = (HttpWebRequest) WebRequest.Create(completeURL);

        HttpWebResponse response = (HttpWebResponse) request.GetResponse();
        
        StreamReader reader = new StreamReader(response.GetResponseStream());
        var json = reader.ReadToEnd();

        json = json.Replace("\"", "");
        json = json.Replace("\"", "");

        return int.Parse(json);
    }
}