﻿using System;

namespace Classes
{
    [Serializable]
    public class DBPlayer
    {
        public int id;
        public string name;
        public int wins;
    }
    
}
