﻿using System;

namespace Classes
{
    [Serializable]
    public class DBLogin
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}