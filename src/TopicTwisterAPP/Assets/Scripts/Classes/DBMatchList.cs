﻿using System.Collections.Generic;

namespace Classes
{
    [System.Serializable]
    public class DBMatchList
    {
        public List<DBMatch> matches;
    }
}