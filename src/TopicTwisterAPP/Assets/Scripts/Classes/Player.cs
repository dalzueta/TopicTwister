﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Classes
{
    [Serializable]
    public class Player
    {
        private int _playerID;
        private string _playerName;
        private int _numberOfVictories;
        
        public Player(string playerName)
        {
            _playerName = playerName;
            _playerID = GetNewPlayerID();
        }

        public Player(string playerName, int playerID)
        {
            _playerName = playerName;
            _playerID = playerID;
        }

        public int GetPlayerID()
        {
            return _playerID;
        }

        public string GetPlayerName()
        {
            return _playerName;
        }

        public int GetPlayerVictories()
        {
            return _numberOfVictories;
        }

        public void IncreaseVictories()
        {
            _numberOfVictories++;
        }

        private int GetNewPlayerID()
        {
            int playerID = 0; 
            BinaryFormatter bf = new BinaryFormatter();
            string savePath = Application.dataPath + "/TempData/" + "PlayerID.data";
            
            if (File.Exists(savePath))
            {
                playerID = LoadPlayerID(savePath, bf);
            }

            playerID++;
            
            SavePlayerID(savePath, bf, playerID);

            return playerID;
        }

        private static int LoadPlayerID(string savePath, IFormatter bf)
        {
            FileStream loadFile = File.Open(savePath, FileMode.Open);
            int playerID = (int) bf.Deserialize(loadFile);
            loadFile.Close();
            return playerID;
        }

        private void SavePlayerID(string savePath, IFormatter bf, int playerID)
        {
            FileStream saveFile = File.Create(savePath);
            bf.Serialize(saveFile, playerID);
            saveFile.Close();
        }
    }
}
