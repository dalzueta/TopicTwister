﻿using System.Collections.Generic;

namespace Classes
{
    [System.Serializable]
    public class DBRound
    {
        public int roundNumber;
        public string letter;
        public List<string> topics;
        public List<string> playerAWords;
        public List<string> playerBWords;
        public List<string> playerAWordsValidation;
        public List<string> playerBWordsValidation;
        public string turn;
        public string roundWinner;
    }
}