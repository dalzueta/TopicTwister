﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Classes
{
    public class PlayerLoader : IPlayerLoader

    {
        private List<Player> _playersList;
        private readonly BinaryFormatter _bf = new BinaryFormatter();
        private readonly string _savePath;

        public PlayerLoader()
        {
            _savePath = Application.dataPath + "/TempData/" + "Players.data";
            LoadPlayers();
        }

        private void LoadPlayers()
        {
            List<Player> playerList;

            if (File.Exists(_savePath))
            {
                FileStream file = File.Open(_savePath, FileMode.Open);
                playerList = (List<Player>)_bf.Deserialize(file);
                file.Close();
            }
            else
            {
                playerList = new List<Player>();
            }

            _playersList = playerList;
        }

        public Player GetPlayer(string name)
        {
            foreach (Player player in _playersList)
            {
                if (player.GetPlayerName() == name)
                {
                    return player;
                }
            }

            Player newPlayer = new Player(name);
            _playersList.Add(newPlayer);
            SavePlayers();

            return newPlayer;
        }

        public List<Player> GetPlayersList()
        {
            return _playersList;
        }

        private void SavePlayers()
        {
            FileStream file = File.Create(_savePath);
            _bf.Serialize(file, _playersList);
            file.Close();
        }

        public void LoginPlayer(LoginHandler credentials)
        {
            throw new System.NotImplementedException();
        }
    }
}
