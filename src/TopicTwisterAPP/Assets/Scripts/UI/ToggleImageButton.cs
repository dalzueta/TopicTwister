﻿using UnityEngine;

namespace UI
{
    public class ToggleImageButton : MonoBehaviour
    {
        [SerializeField] private GameObject image;
        private bool isActive;
    
        public void ButtonPress()
        {
            if (isActive)
            {
                image.gameObject.SetActive(false);
                isActive = false;
            }
            else
            {
                image.gameObject.SetActive(true);
                isActive = true;
            }
        }
    }
}
