﻿namespace UI.Scenes.Game
{
    public interface IGameTimer
    {
        void StartTimer();
        void SaveTimer();
        void LoadTimer();
        void UpdateTimer(int updatedTime);
        void EndTimerActions();
    }
}