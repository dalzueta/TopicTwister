﻿using System.Collections;
using System.Collections.Generic;
using Classes;
using UnityEngine;
using UniRx;
using UnityEngine.SceneManagement;

namespace UI.Scenes.Game
{
    public class GamePresenter : IGamePresenter, IGameTimer
    {
        private readonly IGameView _view;
        private readonly InputBoxController[] _inputBoxControllers;
        private readonly List<string> _wordList = new List<string>();
        private readonly RequestsHandler _requestsHandler = new RequestsHandler();
        private ReactiveProperty<int> _currentTime = new ReactiveProperty<int>(0);
        private int _minimumTime = 15;
        

        public GamePresenter(IGameView view)
        {
            _view = view;
            _inputBoxControllers = _view.GetInputBoxControllers();
            StartPresenter();
        }

        public void StartPresenter()
        {
            SetTopicsInView();
            SetLetterOnView();
            StartTimer();
            StartTimeSubscriber();
        }

        private void StartTimeSubscriber()
        {
            _currentTime.Subscribe(newTime => UpdateTimerOnView(newTime.ToString()));
            _currentTime.Where(time => time == 0).Subscribe(x => ValidateWords());
        }

        private void UpdateTimerOnView(string newTime)
        {
            _view.SetTimerText(newTime);
        }
        
        

        #region StartPresenterMethods

        private void SetTopicsInView()
        {
            for (int index = 0; index < _inputBoxControllers.Length; index++)
            {
                _inputBoxControllers[index].SetTopicText(DataManager.CurrentMatch.GetRoundToPlay().topics[index]);
            }
        }

        private void SetLetterOnView()
        {
            _view.SetLetter(DataManager.CurrentMatch.GetRoundToPlay().letter);
        }

        #endregion

        public void ValidateWords()
        {
            _view.ViewStoppingCoroutine();
            
            SaveTimer();
            
            _view.SetValidateButtonNonInteractable();
            
            AddWordsFromInputs();

            bool[] validationPlayer = ConvertListToBoolArray(SetValidationDataFromCurrentPlayer());

            DisplayCorrectOrWrongImageInInputBoxControllers(validationPlayer);
            
            StartScreenChangeCoroutine();
        }

        #region ValidateWordsMethods

        private void AddWordsFromInputs()
        {
            foreach (InputBoxController inputBoxController in _inputBoxControllers)
            {
                string word = inputBoxController.GetInput();
                _wordList.Add(word);
            }

            DataManager.CurrentMatch = _requestsHandler.AddWordsToMatch(_wordList);
        }

        private bool[] ConvertListToBoolArray(List<string> validationList)
        {
            bool[] bools = new bool[validationList.Count];
            for (int index = 0; index < validationList.Count; index++)
            {
                if (validationList[index] == "true")
                {
                    bools[index] = true;
                }
                else
                {
                    bools[index] = false;
                }
            }

            return bools;
        }

        private List<string> SetValidationDataFromCurrentPlayer()
        {
            var isplayerA = DataManager.IsPlayerA();
            var currentRound = DataManager.CurrentMatch.GetPlayedRoundNumber();
            if (DataManager.CurrentMatch.rounds[0].roundWinner == DataManager.CurrentMatch.rounds[1].roundWinner && DataManager.CurrentMatch.rounds[1].turn == "FINISHED" )
            {
                currentRound = 1;
            }
            var playerAwordsValidation = DataManager.CurrentMatch.rounds[currentRound].playerAWordsValidation;
            var playerBwordsValidation = DataManager.CurrentMatch.rounds[currentRound].playerBWordsValidation;
            return isplayerA ? playerAwordsValidation : playerBwordsValidation;
        }
        

        private void DisplayCorrectOrWrongImageInInputBoxControllers(bool[] validationPlayer)
        {
            for (int i = 0; i < _inputBoxControllers.Length; i++)
            {
                _inputBoxControllers[i].DisplayValidImage(validationPlayer[i]);
            }
        }

        private void StartScreenChangeCoroutine()
        {
            _view.ViewStartingCoroutine(ScreenChange());
        }
        
        private IEnumerator ScreenChange()
        {
            for (int i = 5; i > 0; i--)
            {
                _view.SetButtonText("" + i + "..."); 
                yield return new WaitForSeconds(1);
            }
            SceneManager.LoadScene("Score");
        }

        #endregion

        private IEnumerator DecreaseTimer()
        {
            for (int i = _currentTime.Value; i > 0; i--)
            {
                UpdateTimer(i);
                yield return new WaitForSeconds(1);
            }
            UpdateTimer(0);
            //EndTimerActions();
        }
        
        public void StartTimer()
        {
            LoadTimer();
            _view.ViewStartingCoroutine(DecreaseTimer());
        }

        public void SaveTimer()
        {
            if (IsFirstTurn())
            {
                _requestsHandler.SaveTimeInDB(int.Parse(DataManager.CurrentMatch.matchid.ToString() +
                                                        DataManager.CurrentMatch.GetRoundNumberToPlay()), CalculatePlayedTime());
            }
        }

        private int CalculatePlayedTime()
        {
            return 60 - _currentTime.Value;
        }

        public void LoadTimer()
        {
            if (IsFirstTurn())
            {
                _currentTime.Value = 60;
            }
            else
            {
                _currentTime.Value = _requestsHandler.LoadTimeFromDB(
                    int.Parse(DataManager.CurrentMatch.matchid.ToString() +
                              DataManager.CurrentMatch.GetRoundNumberToPlay()));

                IfRoundTimeIsLessThanMinimumSetMinimumTime();
            }
        }

        private void IfRoundTimeIsLessThanMinimumSetMinimumTime()
        {
            if (_currentTime.Value < _minimumTime)
            {
                _currentTime.Value = _minimumTime;
            }
        }

        private static bool IsFirstTurn()
        {
            return DataManager.CurrentMatch.GetRoundToPlay().turn == "FIRST";
        }

        public void UpdateTimer(int updatedTime)
        {
            UpdateCurrentTime(updatedTime);
            //SetNewTimeOnView(updatedTime);
        }

        private void UpdateCurrentTime(int updatedTime)
        {
            _currentTime.Value = updatedTime;
        }

        private void SetNewTimeOnView(int updatedTime)
        {
            _view.SetTimerText(updatedTime.ToString());
        }

        public void EndTimerActions()
        {
            ValidateWords();
        }
    }
}
