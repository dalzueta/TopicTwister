﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Scenes.Game
{
    public class GameView : MonoBehaviour, IGameView
    {
        private IGamePresenter _presenter;
        [SerializeField] private InputBoxController[] inputBoxControllers = new InputBoxController[5];
        [SerializeField] private TMP_Text letterBox;
        [SerializeField] private GameObject optionsMenuPrefab;
        [SerializeField] private GameObject ui;
        [SerializeField] private Button validateWords;
        [SerializeField] private TMP_Text buttonText;
        [SerializeField] private TMP_Text timerText;

        private Coroutine _timeStoppingCoroutine;

        private void Awake()
        {
            _presenter = new GamePresenter(this);
        }

        public void SetLetter(string letter)
        {
            letterBox.text = letter;
        }

        public void SetTimerText(string time)
        {
            timerText.text = time;
        }

        public void OpenOptionsMenu()
        {
            Instantiate(optionsMenuPrefab, ui.transform);
        }

        public void ValidateWords()
        {
            _presenter.ValidateWords();
        }

        public void SetValidateButtonNonInteractable()
        {
            validateWords.interactable = false;
        }

        public InputBoxController[] GetInputBoxControllers()
        {
            return inputBoxControllers;
        }

        public void SetButtonText(string message)
        {
            buttonText.text = message;
        }
        public void ViewStartingCoroutine(IEnumerator enumerator)
        {
            _timeStoppingCoroutine = StartCoroutine(enumerator);
        }

        public void ViewStoppingCoroutine()
        {
            StopCoroutine(_timeStoppingCoroutine);
        }
    }
}
