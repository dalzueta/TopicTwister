﻿namespace UI.Scenes.Game
{
    public interface IGamePresenter
    {
        void ValidateWords();
        void StartPresenter();
    }
}
