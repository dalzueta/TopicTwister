﻿using System.Collections;
using UI.Scenes.Game;

namespace UI.Scenes.Game
{
    public interface IGameView
    {
        void SetLetter(string letter);
        InputBoxController[] GetInputBoxControllers();
        void SetButtonText(string message);
        void ViewStartingCoroutine(IEnumerator enumerator);

        void SetTimerText(string time);
        void ValidateWords();
        void SetValidateButtonNonInteractable();

        void ViewStoppingCoroutine();
    }
}
