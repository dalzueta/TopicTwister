﻿using TMPro;
using UnityEngine;

namespace UI.Scenes.Game
{
    public class InputBoxController : MonoBehaviour
    {
        [SerializeField] private TMP_Text topicText;
        [SerializeField] private TMP_InputField inputBox;
        [SerializeField] private GameObject correctImage;
        [SerializeField] private GameObject wrongImage;

        public string GetInput()
        {
            return inputBox.text;
        }

        public void SetTopicText(string text)
        {
            topicText.text = text;
        }

        public string GetTopicText()
        {
            return topicText.text;
        }

        public void DisplayValidImage(bool validation)
        {
            ResetImages();
            if (validation)
            {
                correctImage.gameObject.SetActive(true);
            }
            else
            {
                wrongImage.gameObject.SetActive(true);
            }
        }

        private void ResetImages()
        {
            correctImage.gameObject.SetActive(false);
            wrongImage.gameObject.SetActive(false);
        }
    }
}
