﻿namespace UI.Scenes.MainMenu
{
    public class LoginPopupObserver : IObserver
    {
        private IMainMenuView _view;
        public LoginPopupObserver(IMainMenuView view)
        {
            _view = view;
        }
        public void ReceiveUpdate()
        {
            _view.UpdateByLoginPopup();
        }
    }
}