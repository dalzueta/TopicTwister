﻿using Classes;
using TMPro;
using UnityEngine;

namespace UI.Scenes.MainMenu
{
    public class MainMenuView : MonoBehaviour, IMainMenuView, IObserver
    {
        [SerializeField] private GameObject observerGO;
        [SerializeField] private GameObject matchesContainer;
        [SerializeField] private GameObject matchContainer;
        [SerializeField] private GameObject loginPopup;
        [SerializeField] private GameObject canvas;
        [SerializeField] private TMP_Text playerName;
        private IMainMenuPresenter _presenter;
        private LoginPopupObserver _loginPopupObserver;

        private GameObject _loginPopupGO;

        private void Awake()
        {
            _presenter = new MainMenuPresenter(this);
            _loginPopupObserver = new LoginPopupObserver(this);
        }

        private void Start()
        {
            if (IsPlayerNotLoggedIn())
            {
                InstantiateLoginPopup();
            }
            else
            {
                UpdateByLoginPopup();
            }

            observerGO.GetComponent<UpdateContentEvent>().Attach(this);
        }

        private bool IsPlayerNotLoggedIn()
        {
            return DataManager.CurrentPlayer == null;
        }

        public void InstantiateLoginPopup()
        {
            _loginPopupGO = Instantiate(loginPopup, canvas.transform);
            _loginPopupGO.GetComponent<ReactiveScript>().Attach(_loginPopupObserver);
        }

        public IMatchContainer InstantiateMatchesContainer() 
        {
            GameObject container = Instantiate(matchContainer, matchesContainer.transform);
            return container.GetComponent<MatchContainer>();
        }

        public void SetPlayerName(string newName)
        {
            playerName.text = newName;
        }

        public void DestroyContainer()
        {
            foreach (Transform child in matchesContainer.transform)
            {
                Destroy(child.gameObject);
            }
        }

        public void UpdateByLoginPopup()
        {
            playerName.text = DataManager.CurrentPlayer.name;
            _presenter.LoadMatches();
        }

        public void LogOut()
        {
            _presenter.Logout();
        }

        public void PlayNewGame()
        {
            _presenter.PlayNewGame();
        }

        public void ReceiveUpdate()
        {
            _presenter.LoadMatches();
        }
    }
}

