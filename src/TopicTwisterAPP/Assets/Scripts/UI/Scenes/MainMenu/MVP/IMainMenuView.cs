﻿namespace UI.Scenes.MainMenu
{
    public interface IMainMenuView
    {
        IMatchContainer InstantiateMatchesContainer();
        void SetPlayerName(string newName);
        
        void DestroyContainer();
        void UpdateByLoginPopup();
        void LogOut();
        void InstantiateLoginPopup();
    }
}
