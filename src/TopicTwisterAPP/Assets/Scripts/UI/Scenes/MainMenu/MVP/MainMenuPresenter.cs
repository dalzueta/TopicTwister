﻿using System;
using System.Collections.Generic;
using System.Linq;
using Classes;
using UnityEngine.SceneManagement;

namespace UI.Scenes.MainMenu
{
    public class MainMenuPresenter : IMainMenuPresenter

    {
        private readonly IMainMenuView _view;
        private RequestsHandler _requestsHandler = new RequestsHandler();
        private List<DBMatch> _matches;
        private IMatchContainer[] _matchContainers = new IMatchContainer[3];
        private IDisposable _setPlayerNameDisposable;

        public MainMenuPresenter(IMainMenuView view)
        {
            _view = view;
        }
        
        public void LoadMatches()
        {
            DestroyExistingMatchContainers();
            if (DataManager.CurrentPlayer != null)
            {
                _matches = LoadMatchesFromDB();
            }
            
            if (AreNoMatches()) return;
            
            FilterMatchesList(_matches);
        }

        private void DestroyExistingMatchContainers()
        {
            _view.DestroyContainer();
        }

        private List<DBMatch> LoadMatchesFromDB()
        {
            return _requestsHandler.GetAllMatchesFromPlayer(DataManager.CurrentPlayer.id);
        }

        private bool AreNoMatches()
        {
            return _matches.Count == 0;
        }

        private void FilterMatchesList(List<DBMatch> matches)
        {
            List<DBMatch> playMatches = new List<DBMatch>();
            List<DBMatch> waitingMatches = new List<DBMatch>();
            List<DBMatch> finishedMatches = new List<DBMatch>();
            
            foreach (DBMatch match in matches)
            {

                if (match.IsFinished()) { finishedMatches.Add(match); continue; }
                
                if (AmITheCurrentPlayer(match))
                {
                    playMatches.Add(match);
                }
                else
                {
                    waitingMatches.Add(match);
                }
            }

            CheckIfThereArePlayMatchesAndSetUpThem(playMatches);
            CheckIfThereAreWaitingMatchesAndSetUpThem(waitingMatches);
            CheckIfThereAreFinishedMatchesAndSetUpThem(finishedMatches);
        }

        #region FilterMatchesMethods

        private bool AmITheCurrentPlayer(DBMatch match)
        {
            return match.GetCurrentPlayer() == DataManager.CurrentPlayer.id;
        }
        
        private void CheckIfThereArePlayMatchesAndSetUpThem(List<DBMatch> playMatches)
        {
            if (playMatches.Any()) SetupPlayMatches(playMatches);
        }
        private void SetupPlayMatches(List<DBMatch> playMatches)
        {
            _matchContainers[0] = _view.InstantiateMatchesContainer();
            _matchContainers[0].SetMatchContainer(playMatches,MatchContainer.MatchType.Play);
        }
        
        private void CheckIfThereAreWaitingMatchesAndSetUpThem(List<DBMatch> waitingMatches)
        {
            if (waitingMatches.Any()) SetupWaitingMatches(waitingMatches);
        }
        private void SetupWaitingMatches(List<DBMatch> waitingMatches)
        {
            _matchContainers[1] = _view.InstantiateMatchesContainer();
            _matchContainers[1].SetMatchContainer(waitingMatches, MatchContainer.MatchType.Waiting);
        }
        
        private void CheckIfThereAreFinishedMatchesAndSetUpThem(List<DBMatch> finishedMatches)
        {
            if (finishedMatches.Any()) SetupFinishedMatches(finishedMatches);
        }
        private void SetupFinishedMatches(List<DBMatch> finishedMatches)
        {
            _matchContainers[2] = _view.InstantiateMatchesContainer();
            _matchContainers[2].SetMatchContainer(finishedMatches, MatchContainer.MatchType.Finished);
        }

        #endregion

        public void PlayNewGame()
        {
            ChangeSceneToMatchmaking();
        }

        private void ChangeSceneToMatchmaking()
        {
            SceneManager.LoadScene("Matchmaking");
        }

        public void Logout()
        {
            SetCurrentPlayerAndMatchToNull();
            DestroyExistingMatchContainers();
            InstantiateLoginPopupInView();
        }

        private void InstantiateLoginPopupInView()
        {
            _view.InstantiateLoginPopup();
        }

        private void SetCurrentPlayerAndMatchToNull()
        {
            DataManager.CurrentMatch = null;
            DataManager.CurrentPlayer = null;
        }
    }
}
