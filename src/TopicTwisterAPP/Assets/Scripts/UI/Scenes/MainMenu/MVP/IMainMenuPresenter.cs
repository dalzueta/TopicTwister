﻿namespace UI.Scenes.MainMenu
{
    public interface IMainMenuPresenter
    {
        void PlayNewGame();

        void LoadMatches();

        void Logout();
    }
}
