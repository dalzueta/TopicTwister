﻿using UnityEngine;
using TMPro;
using Classes;
using UnityEngine.SceneManagement;

namespace UI.Scenes.MainMenu 
{
    public class GameEntry : MonoBehaviour
    {
        [SerializeField] private TMP_Text opponentName;
        [SerializeField] private TMP_Text language;
        [SerializeField] private TMP_Text round;
        [SerializeField] private TMP_Text points;
        [SerializeField] private GameObject button;
        [SerializeField] private TMP_Text buttonText;
        [SerializeField] private GameObject waitingImage;
        private DBMatch _match;
        private MatchContainer.MatchType _type;
        private RequestsHandler _requestsHandler = new RequestsHandler();

        public void SetMatch(DBMatch match, MatchContainer.MatchType type) 
        {
            _match = match;
            _type = type;
            SetLanguage();
            SetRound();
            SetScore();
            SetOpponentName();

            switch (_type)
            {
                case MatchContainer.MatchType.Play:
                    SetMatchPlay();
                    break;
                case MatchContainer.MatchType.Waiting:
                    SetMatchWaiting();
                    break;
                case MatchContainer.MatchType.Finished:
                    SetMatchFinished();
                    break;
            }
        }

        #region SetMatchMethods

        private void SetLanguage()
        {
            language.text = "EN";
        }

        private void SetRound()
        {
            int roundNumber = _match.GetPlayedRoundNumber() + 1;
            round.text = "Round " + roundNumber;
        }

        private void SetScore()
        {
            points.text = _match.GetScore(DataManager.CurrentPlayer.id);
        }

        private void SetOpponentName()
        {
            if (_match.IsPlayerA(DataManager.CurrentPlayer.id))
            {
                SetOpponentPlayerB();
            }
            else
            {
                SetOpponentPlayerA();
            }
        }

        private void SetOpponentPlayerA()
        {
            opponentName.text = _match.playerAID == 0 ? "Waiting for Opponent" :  _requestsHandler.GetPlayerName(_match.playerAID).name;
        }

        private void SetOpponentPlayerB()
        {
            opponentName.text = _match.playerBID == 0 ? "Waiting for Opponent" : _requestsHandler.GetPlayerName(_match.playerBID).name;
        }

        private void SetMatchPlay()
        {
            buttonText.text = "PLAY";
            button.SetActive(true);
        }

        private void SetMatchWaiting()
        {
            button.SetActive(false);
            waitingImage.SetActive(true);
        }

        private void SetMatchFinished()
        {
            buttonText.text = "REMATCH";
            button.SetActive(true);
        }

        #endregion

        public void StartMatch()
        {
            DataManager.CurrentMatch = _match;
            SceneManager.LoadScene(_type == MatchContainer.MatchType.Finished ? "Matchmaking" : "Topics");
        }
    }
}