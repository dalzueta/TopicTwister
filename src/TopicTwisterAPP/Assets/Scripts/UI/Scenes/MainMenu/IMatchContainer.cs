﻿using System.Collections.Generic;
using Classes;

namespace UI.Scenes.MainMenu
{
    public interface IMatchContainer
    {
        void SetMatchContainer(List<DBMatch> matchesList, MatchContainer.MatchType titleName);
    }
}