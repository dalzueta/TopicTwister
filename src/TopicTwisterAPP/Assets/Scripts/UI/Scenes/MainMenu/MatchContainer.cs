﻿using System;
using System.Collections.Generic;
using Classes;
using TMPro;
using UnityEngine;

namespace UI.Scenes.MainMenu
{
    public class MatchContainer : MonoBehaviour, IMatchContainer
    {
        [SerializeField] private GameObject gameEntry;
        [SerializeField] private TMP_Text titleText;
        private List<DBMatch> _matchesList;
        
        public enum MatchType
        {
            Play,
            Waiting,
            Finished
        }

        public void SetMatchContainer(List<DBMatch> matchesList, MatchType type)
        {
            _matchesList = matchesList;
            SetTitle(type);
            InstantiateMatches(type);
        }

        private void SetTitle(MatchType type)
        {
            switch (type)
            {
                case MatchType.Play:
                    titleText.text = "Your turn!";
                    break;
                case MatchType.Waiting:
                    titleText.text = "Their turn...";
                    break;
                case MatchType.Finished:
                    titleText.text = "Finished matches";
                    break;
            }
        }

        private void InstantiateMatches(MatchType type) 
        {
            foreach (DBMatch match in _matchesList)
            {
                GameObject matchContainer = Instantiate(gameEntry, gameObject.transform);
                GameEntry gameEntryScript = matchContainer.GetComponent<GameEntry>();
                gameEntryScript.SetMatch(match, type);
            }
        }

        public void DestroyMatchContainer()
        {
            Destroy(gameObject);
        }
    }
}