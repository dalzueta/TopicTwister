﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Scenes.MainMenu
{
    public class UpdateContentEvent : MonoBehaviour, ISubject
    {
        [SerializeField] private GameObject gameObjectToControl;
        [SerializeField] private float objectivePosY;
        private float resetPosY = 450f;
        private bool _isUpdated = false;

        private List<IObserver> _observers = new List<IObserver>();
        void Update()
        {
            if (IsReset()) _isUpdated = false; 
            if (_isUpdated) return; 
            if (!IsObjectiveReached()) return;
            _isUpdated = true;
            Notify();
        }

        private bool IsObjectiveReached()
        {
            return gameObjectToControl.transform.localPosition.y < resetPosY;
        }

        private bool IsReset()
        {
            return gameObjectToControl.transform.localPosition.y > resetPosY;
        }

        public void Attach(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (IObserver observer in _observers)
            {
                observer.ReceiveUpdate();
            }
        }
    }
}
