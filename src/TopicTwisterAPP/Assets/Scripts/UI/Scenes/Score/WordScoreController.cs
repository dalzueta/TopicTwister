﻿using TMPro;
using UnityEngine;

namespace UI.Scenes.Score
{
    public class WordScoreController : MonoBehaviour, IWordScoreController
    {
        [SerializeField] private GameObject player2;
        [SerializeField] private TMP_Text topicText;
        [SerializeField] private TMP_Text wordPlayerA;
        [SerializeField] private TMP_Text wordPlayerB;
        [SerializeField] private GameObject correctImagePlayerA;
        [SerializeField] private GameObject wrongImagePlayerA;
        [SerializeField] private GameObject correctImagePlayerB;
        [SerializeField] private GameObject wrongImagePlayerB;

        public void SetTopicText(string topicName)
        {
            topicText.text = topicName;
        }

        public string GetTopicText()
        {
            return topicText.text;
        }
        
        public void SetWordPlayerA(string word)
        {
            wordPlayerA.text = word;
        }

        public string GetWordPlayerA()
        {
            return wordPlayerA.text;
        }
        public void SetWordPlayerB(string word)
        {
            wordPlayerB.text = word;
        }

        public string GetWordPlayerB()
        {
            return wordPlayerB.text;
        }

        public void SetValidPlayerA(bool isValid)
        {
            if (isValid)
            {
                correctImagePlayerA.SetActive(true);
                wrongImagePlayerA.SetActive(false);
            }
            else
            {
                wrongImagePlayerA.SetActive(true);
                correctImagePlayerA.SetActive(false);
            }
                
        }
        public void SetValidPlayerB(bool isValid)
        {
            if (isValid)
            {
                correctImagePlayerB.SetActive(true);
                wrongImagePlayerB.SetActive(false);
            }
            else
            {
                wrongImagePlayerB.SetActive(true);
                correctImagePlayerB.SetActive(false);
            }
        }

        public void ActivatePlayerB()
        {
            player2.SetActive(true);
        }

        public void DeactivatePlayerB()
        {
            player2.SetActive(false);
        }
        
    }
}
