﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Scenes.Score
{
    public class AvatarController : MonoBehaviour
    {
        [SerializeField] private GameObject avatar;
        [SerializeField] private Image firstRoundImage;
        [SerializeField] private Image secondRoundImage;
        [SerializeField] private Image thirdRoundImage;
        [SerializeField] private TMP_Text playerName;

        public void DeactivateAvatar()
        {
            avatar.SetActive(false);
        }

        public void ActivateAvatar()
        {
            avatar.SetActive(true);
        }

        private void SetScore(Image image, int score)
        {
            switch (score)
            {
                case 0:
                    image.color = Color.red;
                    break;
                case 1:
                    image.color = Color.yellow;
                    break;
                case 2:
                    image.color = Color.green;
                    break;
                default:
                    image.color = Color.white;
                    break;
            }
        }

        public void SetRoundScore(int firstRound, int secondRound, int thirdRound)
        {
            SetFirstRoundScore(firstRound);
            SetSecondRoundScore(secondRound);
            SetThirdRoundScore(thirdRound);
        }

        private void SetFirstRoundScore(int score)
        {
            SetScore(firstRoundImage, score);
        }

        private void SetSecondRoundScore(int score)
        {
            SetScore(secondRoundImage, score);
        }

        private void SetThirdRoundScore(int score)
        {
            SetScore(thirdRoundImage, score);
        }

        public void SetPlayerName(string playerName)
        {
            this.playerName.text = playerName;
        }
        
        
    }
}
