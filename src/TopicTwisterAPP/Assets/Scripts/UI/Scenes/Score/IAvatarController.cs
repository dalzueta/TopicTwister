﻿namespace UI.Scenes.Score
{
    public interface IAvatarController
    {
        void ActivateAvatar();
        void DeactivateAvatar();
    }
}