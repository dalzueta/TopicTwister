﻿namespace UI.Scenes.Score
{
    public interface IWordScoreController
    {
        void SetWordPlayerA(string word);
        void SetWordPlayerB(string word);
        void ActivatePlayerB();
        void DeactivatePlayerB();
        void SetTopicText(string topicName);
        void SetValidPlayerA(bool isValid);
        void SetValidPlayerB(bool isValid);
    }
}