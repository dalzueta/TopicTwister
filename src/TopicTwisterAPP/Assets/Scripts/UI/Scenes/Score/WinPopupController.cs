﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WinPopupController : MonoBehaviour
{
    [SerializeField] private TMP_Text playerName;

    public void SetPlayerName(string name)
    {
        playerName.text = name;
    }
}
