﻿using System;
using System.Collections;
using System.Collections.Generic;
using Classes;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Scenes.Score
{
    public class ScorePresenter : IScorePresenter
    {
        private IScoreView _view;
        private IWordScoreController[] _wordScoreControllers;
        private RequestsHandler _requestsHandler = new RequestsHandler();

        public ScorePresenter(IScoreView view)
        {
            _view = view;
            _wordScoreControllers = _view.GetWordScoreControllers();
        }

        public void StartPresenter()
        {
            PopulateWordScoreController();
            SetScorePoints();
            SetPlayerNames();
        }

        private void SetPlayerNames()
        {
            if (DataManager.IsPlayerA())
            {
                SetPlayerNameInFirstAvatar(DataManager.CurrentMatch.playerAID);
                SetPlayerNameInSecondAvatar(DataManager.CurrentMatch.playerBID);
            }
            else
            {
                SetPlayerNameInFirstAvatar(DataManager.CurrentMatch.playerBID);
                SetPlayerNameInSecondAvatar(DataManager.CurrentMatch.playerAID);
            }
        }

        private void SetPlayerNameInFirstAvatar(int playerID)
        {
            DBPlayer dbPlayer = _requestsHandler.GetPlayerName(playerID);
            _view.SetNameInFirstAvatar(dbPlayer.name);
        }

        private void SetPlayerNameInSecondAvatar(int playerID)
        {
            if (playerID == 0) return;
            DBPlayer dbPlayer = _requestsHandler.GetPlayerName(playerID);
            _view.SetNameInSecondAvatar(dbPlayer.name);

        }


        private void PopulateWordScoreController()
        {
            if (IsFirstTurnPlayed())
            {
                if (IsRoundOne()) DeactivateSecondPlayerAvatarInView();
                
                DeactivatePlayerBWords();
                PopulateControllerWithPlayerAWordsAndTopics();
            }
            else
            {
                PopulateControllerWithPlayerAWordsAndTopics();
                PopulateControllerWithPlayerBWords();
            }
        }

        private bool IsFirstTurnPlayed()
        {
            var result = DataManager.CurrentMatch.GetPlayedRound().turn == "SECOND";
            return result;
        }

        private bool IsRoundOne()
        {
            var result = DataManager.CurrentMatch.GetPlayedRoundNumber() == 0;
            return result;
        }

        private void DeactivateSecondPlayerAvatarInView()
        {
            _view.DeactivateSecondPlayerAvatar();
        }

        private void DeactivatePlayerBWords()
        {
            foreach (IWordScoreController wordScoreController in _wordScoreControllers)
            {
                wordScoreController.DeactivatePlayerB();
            }
        }

        private void PopulateControllerWithPlayerAWordsAndTopics()
        {
            var currentRound = DataManager.CurrentMatch.GetPlayedRoundNumber();
            if (DataManager.CurrentMatch.rounds[0].roundWinner == DataManager.CurrentMatch.rounds[1].roundWinner && DataManager.CurrentMatch.rounds[1].turn == "FINISHED" )
            {
                currentRound = 1;
            }

            List<string> topics = DataManager.CurrentMatch.rounds[currentRound].topics;
            bool[] playerAWordsValidation = DataManager.IsPlayerA() ?
                ConvertListToBoolArray(DataManager.CurrentMatch.rounds[currentRound].playerAWordsValidation) :
                ConvertListToBoolArray(DataManager.CurrentMatch.rounds[currentRound].playerBWordsValidation);
            for (int i = 0; i < _wordScoreControllers.Length; i++)
            {
                List<string> wordsPlayerA = DataManager.IsPlayerA() ? 
                    DataManager.CurrentMatch.rounds[currentRound].playerAWords : 
                    DataManager.CurrentMatch.rounds[currentRound].playerBWords;
                _wordScoreControllers[i].SetWordPlayerA(wordsPlayerA[i]);
                _wordScoreControllers[i].SetTopicText(topics[i]);
                _wordScoreControllers[i].SetValidPlayerA(playerAWordsValidation[i]);
            }
        }

        private void PopulateControllerWithPlayerBWords()
        {
            var currentRound = DataManager.CurrentMatch.GetPlayedRoundNumber();
            if (DataManager.CurrentMatch.rounds[0].roundWinner == DataManager.CurrentMatch.rounds[1].roundWinner && DataManager.CurrentMatch.rounds[1].turn == "FINISHED" )
            {
                currentRound = 1;
            }
            bool[] playerBWordsValidation = DataManager.IsPlayerA() ? 
                ConvertListToBoolArray(DataManager.CurrentMatch.rounds[currentRound].playerBWordsValidation) :
                ConvertListToBoolArray(DataManager.CurrentMatch.rounds[currentRound].playerAWordsValidation);
            for (int i = 0; i < _wordScoreControllers.Length; i++)
            {
                List<string> wordsPlayerB = DataManager.IsPlayerA() ? DataManager.CurrentMatch.rounds[currentRound].playerBWords : DataManager.CurrentMatch.rounds[currentRound].playerAWords;
                _wordScoreControllers[i].SetWordPlayerB(wordsPlayerB[i]);
                _wordScoreControllers[i].SetValidPlayerB(playerBWordsValidation[i]);
            }
        }
        
        private bool[] ConvertListToBoolArray(List<string> validationList)
        {
            bool[] bools = new bool[validationList.Count];
            for (int index = 0; index < validationList.Count; index++)
            {
                if (validationList[index] == "true")
                {
                    bools[index] = true;
                }
                else
                {
                    bools[index] = false;
                }
            }

            return bools;
        }

        private void SetScorePoints()
        {
            SetScoreInAvatars();
        }

        private void SetScoreInAvatars()
        {
            SetScoreInMyAvatar();
            SetScoreInOpponentAvatar();
        }

        private void SetScoreInMyAvatar()
        {
            _view.GetFirstPlayerAvatarController().SetRoundScore(
                ConvertMyScoreToInt(DataManager.CurrentMatch.rounds[0].roundWinner), 
                ConvertMyScoreToInt(DataManager.CurrentMatch.rounds[1].roundWinner), 
                ConvertMyScoreToInt(DataManager.CurrentMatch.rounds[2].roundWinner)
                );
        }
        
        private void SetScoreInOpponentAvatar()
        {
            _view.GetSecondPlayerAvatarController().SetRoundScore(
                ConvertOpponentScoreToInt(DataManager.CurrentMatch.rounds[0].roundWinner), 
                ConvertOpponentScoreToInt(DataManager.CurrentMatch.rounds[1].roundWinner), 
                ConvertOpponentScoreToInt(DataManager.CurrentMatch.rounds[2].roundWinner)
            );
        }

        private int ConvertMyScoreToInt(string score)
        {
            return DataManager.IsPlayerA() ? SetScoreIfImPlayerA(score) : SetScoreIfImPlayerB(score);
        }

        private static int SetScoreIfImPlayerA(string score)
        {
            switch (score)
            {
                case "PLAYERA": return 2;
                case "PLAYERB": return 0;
                case "DRAW": return 1;
                default: return 3;
            }
        }

        private int ConvertOpponentScoreToInt(string score)
        {
            return DataManager.IsPlayerA() ? SetScoreIfImPlayerB(score) : SetScoreIfImPlayerA(score);
        }

        private static int SetScoreIfImPlayerB(string score)
        {
            switch (score)
            {
                case "PLAYERA": return 0;
                case "PLAYERB": return 2;
                case "DRAW": return 1;
                default: return 3;
            }
        }

        public void EndTurn()
        {
            if (DataManager.CurrentMatch.GetRoundToPlay().turn == "FIRST")
            { 
                SceneManager.LoadScene("Topics");
            }
            else
            {
                if (DataManager.CurrentMatch.IsFinished())
                {
                    ShowWinner();
                }
                else
                {
                    ReturnToMainMenu();
                }
            }
        }

        private async void ShowWinner()
        {
            if (DataManager.CurrentMatch.winner == 0)
            {
                _view.SetWinnerName("It was a draw!");
            }
            else
            {
                _view.SetWinnerName(_requestsHandler.GetPlayerName(DataManager.CurrentMatch.winner).name);
            }
            _view.ActivateLoadingPopup();
            await WaitForFiveSeconds();
            SceneManager.LoadScene("Main Menu");
        }

        private IEnumerator WaitForFiveSeconds()
        {
            yield return UniTask.Delay(TimeSpan.FromSeconds(5)).ToCoroutine();
        }

        
        private void ReturnToMainMenu()
        {
            DataManager.CurrentMatch = null;
            SceneManager.LoadScene("Main Menu");
        }
    }
}
