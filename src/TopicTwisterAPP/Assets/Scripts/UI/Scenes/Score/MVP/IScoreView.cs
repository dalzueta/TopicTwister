﻿using System.Collections;

namespace UI.Scenes.Score
{
    public interface IScoreView
    {
        WordScoreController[] GetWordScoreControllers();
        void DeactivateSecondPlayerAvatar();

        AvatarController GetFirstPlayerAvatarController();

        AvatarController GetSecondPlayerAvatarController();

        void SetNameInFirstAvatar(string name);

        void SetNameInSecondAvatar(string name);

        void SetWinnerName(string name);
        void ActivateLoadingPopup();
        void WaitForMainMenu(IEnumerator enumerator);
    }
}
