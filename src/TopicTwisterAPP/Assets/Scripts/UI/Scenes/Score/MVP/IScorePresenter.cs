﻿namespace UI.Scenes.Score
{
    public interface IScorePresenter
    {
        void StartPresenter();
        void EndTurn();
    }
}
