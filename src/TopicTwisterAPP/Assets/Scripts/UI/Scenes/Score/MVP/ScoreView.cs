﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Scenes.Score
{
    public class ScoreView : MonoBehaviour, IScoreView
    {
        private IScorePresenter _presenter;
        [SerializeField] private WordScoreController[] wordScoreController = new WordScoreController[5];
        [SerializeField] private AvatarController firstPlayerAvatar;
        [SerializeField] private AvatarController secondPlayerAvatar;
        [SerializeField] private GameObject winnerPopup;
        private void Awake()
        {
            _presenter = new ScorePresenter(this);
        }

        private void Start()
        {
            _presenter.StartPresenter();
        }

        public WordScoreController[] GetWordScoreControllers()
        {
            return wordScoreController;
        }

        public void DeactivateSecondPlayerAvatar()
        {
            secondPlayerAvatar.DeactivateAvatar();
        }

        public void EndTurn()
        {
            _presenter.EndTurn();
        }

        public AvatarController GetFirstPlayerAvatarController()
        {
            return firstPlayerAvatar;
        }

        public AvatarController GetSecondPlayerAvatarController()
        {
            return secondPlayerAvatar;
        }

        public void SetNameInFirstAvatar(string name)
        {
            firstPlayerAvatar.SetPlayerName(name);
        }

        public void SetNameInSecondAvatar(string name)
        {
            secondPlayerAvatar.SetPlayerName(name);
        }

        public void SetWinnerName(string name)
        {
            winnerPopup.GetComponent<WinPopupController>().SetPlayerName(name);
        }
        
        public void ActivateLoadingPopup()
        {
            winnerPopup.SetActive(true);
        }

        public void WaitForMainMenu(IEnumerator enumerator)
        {
            StartCoroutine(enumerator);
        }
    }
}
