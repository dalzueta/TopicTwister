﻿using System.Collections;
using System.Collections.Generic;
using Classes;
using UnityEngine;

namespace UI.Scenes.Topics
{
    public interface ITopicView
    {
        void ShowLetter(char newLetter);
        void GameStartingCoroutine(IEnumerator enumerator);
        void DisableGetLetterButton();
        void SetGetLetterButtonText(string message);
        TopicBoxController[] GetTopicArray();
    }
}