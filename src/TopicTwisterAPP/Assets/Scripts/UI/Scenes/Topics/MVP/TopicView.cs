﻿using Classes;
using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace UI.Scenes.Topics
{
    public class TopicView : MonoBehaviour, ITopicView
    {
        [SerializeField] private TMP_Text letter;
        [SerializeField] private Button letterButton;
        [SerializeField] private TMP_Text getLetterButtonText;
        [SerializeField] private TopicBoxController[] topicsArray;

        private ITopicPresenter _presenter;

        private void Awake()
        {
            _presenter = new TopicPresenter(this);
        }

        private void Start()
        {
            _presenter.GetTopics();
        }

        public void GetRandomLetter()
        {
            _presenter.ShowRandomLetter();
        }

        public void ShowLetter(char newLetter)
        {
            letter.text = newLetter.ToString();
        }

        public void DisableGetLetterButton()
        {
            letterButton.interactable = false;
        }

        public void SetGetLetterButtonText(string message)
        {
            getLetterButtonText.text = message;
        }

        public TopicBoxController[] GetTopicArray()
        {
            return topicsArray;
        }

        public void GameStartingCoroutine(IEnumerator enumerator)
        {
            StartCoroutine(enumerator);
        }
    }
}