﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Classes;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Scenes.Topics
{
    public class TopicPresenter : ITopicPresenter
    {   
        private readonly ITopicView _view;
        private List<string> _topicsList;
        private TopicBoxController[] _topicsArray;
        public TopicPresenter(ITopicView view)
        {
            _view = view;
        }

        public void GetTopics()
        {
            GetTopicArrayFromView();
            if (IsSecondRound())
            {
                ShowLetter();
            }

            ShowTopics();
        }

        #region GetTopicsMethods

        private void GetTopicArrayFromView()
        {
            _topicsArray = _view.GetTopicArray();
        }

        private static bool IsSecondRound()
        {
            return DataManager.CurrentMatch.GetRoundToPlay().turn == "SECOND";
        }


        private void ShowLetter()
        {
            SetLetterInView();
            StartGame();
        }

        private void StartGame()
        {
            DisableGetLetterButtonInView();
            StartScreenChangeCoroutine();
        }

        private void ShowTopics()
        {
            for (int index = 0; index < 5; index++)
            {
                _topicsArray[index].SetTopic(DataManager.CurrentMatch.GetRoundToPlay().topics[index]);
            }
        }

        #endregion
        
        public void ShowRandomLetter()
        {
            DisableGetLetterButtonInView();
            SetRandomLetterInView();
            StartScreenChangeCoroutine();
        }

        #region GetRandomLetterMethods


        private void SetRandomLetterInView()
        {
            SetLetterInView();
        }

        #endregion

        #region SharedMethods

        private void SetLetterInView()
        {
            _view.ShowLetter(DataManager.CurrentMatch.GetRoundToPlay().letter[0]);
        }

        private void DisableGetLetterButtonInView()
        {
            _view.DisableGetLetterButton();
        }

        private void StartScreenChangeCoroutine()
        {
            _view.GameStartingCoroutine(ScreenChange());
        }

        private IEnumerator ScreenChange()
        {
            for (int i = 5; i > 0; i--)
            {
                _view.SetGetLetterButtonText("Starting in " + i + "..."); 
                yield return new WaitForSeconds(1);
            }
            SceneManager.LoadScene("Game");
        }
        
        #endregion
    }
}