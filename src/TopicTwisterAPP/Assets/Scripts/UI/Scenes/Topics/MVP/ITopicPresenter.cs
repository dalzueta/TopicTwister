﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UI.Scenes.Topics
{
    public interface ITopicPresenter  
    {
        void ShowRandomLetter();
        void GetTopics();
    }
}