﻿using TMPro;
using UnityEngine;

namespace UI.Scenes.Topics
{
    public class TopicBoxController : MonoBehaviour
    {
        [SerializeField] private TMP_Text topicText;
    
        public string GetTopic()
        {
            return topicText.text;
        }

        public void SetTopic(string topicName)
        {
            topicText.text = topicName;
        }
    }
}
