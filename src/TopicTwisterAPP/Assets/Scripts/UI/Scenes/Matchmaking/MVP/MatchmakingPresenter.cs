﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Classes;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Scenes.Matchmaking
{
    public class MatchmakingPresenter : IMatchMakingPresenter
    {
        private IMatchmakingView _view;
        private readonly RequestsHandler _requestsHandler = new RequestsHandler();

        public MatchmakingPresenter(IMatchmakingView view)
        {
            _view = view;
        }

        public void Matchmake()
        {
            SetMatchAsCurrentMatch(IsRematch() ? StartAndGetRematch() : StartAndGetNewMatch());
            StartScreenChangeCoroutine();
        }

        public void SkipAd()
        {
            LoadTopicScene();
        }

        #region MatchmakeMethods
        
        private void SetMatchAsCurrentMatch(DBMatch match)
        {
            DataManager.CurrentMatch = match;
        }

        private DBMatch StartAndGetRematch()
        {
            return _requestsHandler.StartRematch(GetPlayerID(), GetOpponentID());
        }

        private int GetPlayerID()
        {
            return DataManager.CurrentPlayer.id;
        }

        private int GetOpponentID()
        {
            return DataManager.CurrentMatch.IsPlayerA(DataManager.CurrentPlayer.id) ? GetPlayerBID() : GetPlayerAID();
        }
        
        private int GetPlayerAID()
        {
            return DataManager.CurrentMatch.playerAID;
        }

        private int GetPlayerBID()
        {
            return DataManager.CurrentMatch.playerBID;
        }

        private DBMatch StartAndGetNewMatch()
        {
            return _requestsHandler.StartNewMatch(DataManager.CurrentPlayer.id);
        }

        private bool IsRematch()
        {
            return DataManager.CurrentMatch != null;
        }
        
        private void StartScreenChangeCoroutine()
        {
            _view.GameStartingCoroutine(ScreenChange());
        }
        
        private IEnumerator ScreenChange()
        {
            for (int i = 10; i > 0; i--)
            {
                yield return new WaitForSeconds(1);
            }
            LoadTopicScene();
        }

        private void LoadTopicScene()
        {
            SceneManager.LoadScene("Topics");
        }

        #endregion
    }
}