﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace UI.Scenes.Matchmaking
{
    public class MatchmakingView : MonoBehaviour, IMatchmakingView
    {
        private IMatchMakingPresenter _presenter;
        [SerializeField] private TMP_Text messageBox;

        private void Awake()
        {
            _presenter = new MatchmakingPresenter(this);
        }

        private void Start()
        {
            _presenter.Matchmake();
        }

        public void SetMessage(string message)
        {
            messageBox.text = message;
        }
        
        public void GameStartingCoroutine(IEnumerator enumerator)
        {
            StartCoroutine(enumerator);
        }

        public void SkipAd()
        {
            _presenter.SkipAd();
        }
    }
}
