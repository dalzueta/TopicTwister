﻿namespace UI.Scenes.Matchmaking
{
    public interface IMatchMakingPresenter
    {
        void Matchmake();
        void SkipAd();
    }
}