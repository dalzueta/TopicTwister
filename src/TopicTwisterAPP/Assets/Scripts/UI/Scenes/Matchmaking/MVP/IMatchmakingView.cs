﻿using System.Collections;

namespace UI.Scenes.Matchmaking
{
    public interface IMatchmakingView
    {
        void SetMessage(string message);
        void GameStartingCoroutine(IEnumerator screenChange);
    }
}