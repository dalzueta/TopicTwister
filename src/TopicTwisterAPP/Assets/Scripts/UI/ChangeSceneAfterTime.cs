﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class ChangeSceneAfterTime : MonoBehaviour
    {
        void Awake()
        {
            StartCoroutine(ChangeSceneAfterSeconds(3));
        }

        private IEnumerator ChangeSceneAfterSeconds(float time)
        {
            yield return new WaitForSeconds(time);
            
            SceneManager.LoadScene("Main Menu");
        }
    }
}
