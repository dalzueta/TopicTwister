﻿using TMPro;
using UI.Scenes.MainMenu;
using UnityEngine;

namespace UI.PlayerPopup
{
    public class PlayerLoginView : MonoBehaviour, IPlayerLoginView
    {
        private IPlayerLoginPresenter _presenter;
        
        [SerializeField] private TMP_InputField emailLogin;
        [SerializeField] private TMP_InputField passwordLogin;
        [SerializeField] private TMP_InputField emailRegister;
        [SerializeField] private TMP_InputField passwordRegister;
        [SerializeField] private TMP_InputField nameRegister;
        [SerializeField] private GameObject loadingPopup;

        private ReactiveScript _reactiveScript;

        private void Awake()
        {
            _presenter = new PlayerLoginPresenter(this);
            _reactiveScript = gameObject.GetComponent<ReactiveScript>();
        }

        public void Login()
        {
            _presenter.LoginPlayer(emailLogin.text, passwordLogin.text);
        }

        public void Register()
        {
            _presenter.RegisterPlayer(emailRegister.text, passwordRegister.text, nameRegister.text);
        }

        public void ClosePopup()
        {
            Destroy(gameObject);
        }

        public void SendUpdate()
        {
            _reactiveScript.Notify();
        }

        public void ActivateLoadingPopup()
        {
            loadingPopup.SetActive(true);
        }
        
        public void DeactivateLoadingPopup()
        {
            loadingPopup.SetActive(false);
        }
    }
}
