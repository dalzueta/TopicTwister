﻿namespace UI.PlayerPopup
{
    public interface IPlayerLoginPresenter
    {
        void LoginPlayer(string email, string password);
        void RegisterPlayer(string emailRegisterText, string passwordRegisterText, string nameRegisterText);
    }
}
