﻿using System.Collections.Generic;

namespace UI.PlayerPopup
{
    public interface IPlayerLoginView
    {
        void Login();
        void ClosePopup();
        void SendUpdate();
        void ActivateLoadingPopup();
        void DeactivateLoadingPopup();
    }
}
