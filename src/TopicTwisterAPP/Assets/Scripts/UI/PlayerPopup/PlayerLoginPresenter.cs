﻿using Classes;

namespace UI.PlayerPopup
{
    public class PlayerLoginPresenter : IPlayerLoginPresenter
    {
        private readonly IPlayerLoginView _view;
        private LoginHandler _loginHandler = new LoginHandler();
        private LoginAPICallerUniTask _loginHandlerUniTask = new LoginAPICallerUniTask();

        public PlayerLoginPresenter(IPlayerLoginView view)
        {
            _view = view;
        }

        public async void LoginPlayer(string email, string password)
        {
            _view.ActivateLoadingPopup();
            DataManager.CurrentPlayer = await _loginHandlerUniTask.LoginPlayer(email, password);
            _view.SendUpdate();
            _view.DeactivateLoadingPopup();
            _view.ClosePopup();
        }

        public void RegisterPlayer(string email, string password, string name)
        {
            _view.ActivateLoadingPopup();
            DataManager.CurrentPlayer = _loginHandler.SendRegister(email, password, name);
            _view.SendUpdate();
            _view.DeactivateLoadingPopup();
            _view.ClosePopup();
        }
    }
}
