﻿using UnityEngine;

namespace UI
{
    public class GameObjectRotation : MonoBehaviour
    {
        [SerializeField] private float speed = 1;
        void Update()
        {
            RotateGameObject();
        }
    
        private void RotateGameObject()
        {
            gameObject.transform.Rotate(new Vector3(0f, 0f, 4f * speed), Space.Self);
        }
    }
}
