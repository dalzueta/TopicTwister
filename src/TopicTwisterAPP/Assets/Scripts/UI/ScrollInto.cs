﻿using System;
using System.Collections;
using UnityEngine;

namespace UI
{
    public class ScrollInto : MonoBehaviour
    {
        [SerializeField] private CanvasGroup popup;
        [SerializeField] private Direction directionToSpawn = Direction.Up;

        private enum Direction
        {
            Up,
            Down,
            Left,
            Right
        }
        
        private void OnEnable()
        {
            Scroll();
        }
        
        private void Scroll()
        {
            StartCoroutine(ScrollIn(popup, 0.5f, directionToSpawn));
        }


        private IEnumerator ScrollIn(CanvasGroup window, float time, Direction direction)
        {
            StartCoroutine(FadeIn(window, time));
            Vector3 endPosition = window.gameObject.transform.position;
            Vector3 pos = endPosition;
            switch (direction)
            {
                case Direction.Up:
                    pos.y = Screen.height;
                    break;
                case Direction.Down:
                    pos.y = Screen.height * -1;
                    break;
                case Direction.Left:
                    pos.x = Screen.width * -1;
                    break;
                case Direction.Right:
                    pos.x = Screen.width * 4;
                    break;
            }
        
            for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / time)
            {
                window.gameObject.transform.position = Vector3.Lerp(pos, endPosition, t);
                yield return null;
            }

            window.gameObject.transform.position = endPosition;

        }
        private IEnumerator FadeIn(CanvasGroup window, float time)
        {
            float alpha = window.alpha;
            for (float t = 0.0f; t <= 1.0f; t += Time.deltaTime / time)
            {
                window.alpha = Mathf.Lerp(alpha, 1, t);
                yield return null;
            }
            window.alpha = 1;
        }
    }
}
