# Progress 

* [ ]  Subtask #1
* [ ]  Subtask #2
* [ ]  Subtask #3

# Description

Description of the bug or issue.

# Details

## Steps to reproduce

How the issue can be reproduced.

## Current behavior

What actually happens.

## Expected behavior

What should happen instead.

## Relevant logs and screenshots

Any relevant information regarding the issue or bug.

## Changes

Guide on the changes to fix the bug. 