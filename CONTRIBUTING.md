# Contributing

When contributing to this repository, please follow the next conventions.

<br>

## **Commit Messages**

Commit messages must comply with the following rules:

1. Message length must not be longer than 100 characters
2. Message must not end with a period
3. Message must be written with a predefined structure as follows:

**category - action - description**

**Categories**

- `[git]`
- `[test]`
- `[documentation]`
- `[code-management]`
- `[general]`
- `[unity]`

**Actions**

- `add`
- `fix`
- `remove`
- `update`
- `move`

**Examples**

```shell
git commit -m "[test] - add - unit tests from 20 to 30"
```

```shell
git commit -m "[documentation] - update - update the name convention section"
```

## **Merge Request Descriptions**

The description of a merge request must follow the structure defined in the corresponding template:

- [Features](./docs/contribution-templates/MR-features-template.md)
- [Bugs or Issues](./docs/contribution-templates/MR-bug-or-issue-template.md)
- [Documentation](./docs/contribution-templates/MR-documentation-templates.md)
